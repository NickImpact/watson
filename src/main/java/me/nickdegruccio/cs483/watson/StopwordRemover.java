package me.nickdegruccio.cs483.watson;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class StopwordRemover {

	private static List<String> stopwords = new ArrayList<>();

	public void readStopwords() throws Exception {
		if(stopwords.isEmpty()) {
			File file = new File(getClass().getClassLoader().getResource("stopwords.txt").getFile());
			Scanner scanner = new Scanner(file);
			while (scanner.hasNextLine()) {
				stopwords.add(scanner.nextLine().trim());
			}
		}
	}

	public String removeAllStopWords(String content) {
		for(String stopword : stopwords) {
			content = content.replaceAll("^" + stopword + "$", "");
		}

		return content;
	}
}
