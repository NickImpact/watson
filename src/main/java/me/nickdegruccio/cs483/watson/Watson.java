package me.nickdegruccio.cs483.watson;

import me.nickdegruccio.cs483.watson.jeopardy.JeopardyQuestion;
import me.nickdegruccio.cs483.watson.jeopardy.JeopardyReader;
import me.nickdegruccio.cs483.watson.parsers.Lemma;
import me.nickdegruccio.cs483.watson.parsers.None;
import me.nickdegruccio.cs483.watson.parsers.Parser;
import me.nickdegruccio.cs483.watson.parsers.Stemming;
import me.nickdegruccio.cs483.watson.wiki.WikiReader;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.store.FSDirectory;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.time.Duration;
import java.util.*;

public class Watson {

	public static void main(String[] args) {
		StopwordRemover swr = new StopwordRemover();
		try {
			swr.readStopwords();
		} catch (Exception e) {
			e.printStackTrace();
		}
		JeopardyReader reader = new JeopardyReader();
		List<JeopardyQuestion> questions = null;
		try {
			System.out.println("Reading in jeopardy files...");
			questions = reader.readAll();
			questions.sort(Comparator.comparing(JeopardyQuestion::getCategory));
		} catch (Exception e) {
			System.err.println("Failed to read files, exiting...");
			System.exit(1);
		}

		Map<String, Integer> catCount = new HashMap<>();
		for (JeopardyQuestion q : questions) {
			if (catCount.containsKey(q.getCategory())) {
				catCount.put(q.getCategory(), catCount.get(q.getCategory()) + 1);
			} else {
				catCount.put(q.getCategory(), 1);
			}
		}

		List<Parser> parsers = new ArrayList<>();
		parsers.add(new Stemming());
		parsers.add(new Lemma());
		parsers.add(new None());

		for(Parser parser : parsers) {
			System.out.println("Using " + parser.name() + " parser...");

			try {
				FSDirectory directory = FSDirectory.open(Paths.get(".//index/" + parser.name()));
				WhitespaceAnalyzer analyzer = new WhitespaceAnalyzer();
				if (!Files.exists(Paths.get(String.format("./index/%s/segments_1", parser.name())))) {
					long start = System.currentTimeMillis();
					WikiReader wikiReader = new WikiReader(parser);
					List<Document> documents = new ArrayList<>();
					System.out.println("  Indexing wiki files, this will take some time...");
					try {
						documents = wikiReader.index();
					} catch (Exception e) {
						System.err.println("  Failed to index files...");
						e.printStackTrace();
					}

					System.out.println("  Creating index directory...");
					IndexWriterConfig iwc = new IndexWriterConfig(analyzer).setOpenMode(IndexWriterConfig.OpenMode.CREATE);
					IndexWriter writer = new IndexWriter(directory, iwc);
					writer.addDocuments(documents);
					writer.commit();
					writer.close();

					long end = System.currentTimeMillis();
					System.out.println("  Indexing complete, took: " + duration(Duration.ofMillis(end - start)));
				} else {
					System.out.println("  Found the index, reading from it now...");
				}

				System.out.println("  Attempting to solve clues for each question by category...");
				IndexReader ir = DirectoryReader.open(directory);
				IndexSearcher is = new IndexSearcher(ir);
				//is.setSimilarity(new BooleanSimilarity());
				is.setSimilarity(new BM25Similarity());

				String currentCat = "";
				int correct = 0;

				double rankings = 0;
				for (JeopardyQuestion question : questions) {
					if (!question.getCategory().equals(currentCat)) {
						System.out.println(question.getCategory());
						currentCat = question.getCategory();
					}

					Query query = new QueryParser("content", analyzer).parse(parser.apply(swr.removeAllStopWords(question.getClue())));
					TopDocs top = is.search(query, 1);
					if (top.scoreDocs.length > 0) {
						StringBuilder allAnswers = new StringBuilder();
						boolean found = false;
						int d = 1;
						for (ScoreDoc score : top.scoreDocs) {
							Document doc = is.doc(score.doc);
							String page = doc.get("page").toLowerCase();
							if(page.equals(question.getAnswer().toLowerCase())) {
								++correct;
								found = true;
								break;
							}
							allAnswers.append(doc.get("page")).append(", ");
							++d;
						}

						if(found) {
							rankings += ((double) 1 / d);
						}

						if (!found) {
							System.out.println(String.format("    " + question.getClue() + ": No - Answer = %s, Answered: " + allAnswers, question.getAnswer()));
						} else {
							System.out.println("    " + question.getClue() + ": Yes");
						}
					} else {
						System.out.println("    " + question.getClue() + ": No docs found");
					}
				}

				System.out.println("  Results: " + correct + "/100");
				System.out.println("  MRR: " + new DecimalFormat("#0.0000").format(rankings / 100.0));
				parser.setCorrect(correct);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		System.out.println("Final Results:");
		for(Parser parser : parsers) {
			System.out.println(String.format("%s: %d/100", parser.name(), parser.getCorrect()));
		}
	}

	private static String duration(Duration duration) {
		long seconds = duration.getSeconds();
		long absSeconds = Math.abs(seconds);
		String positive = String.format(
				"%d:%02d:%02d",
				absSeconds / 3600,
				(absSeconds % 3600) / 60,
				absSeconds % 60);
		return seconds < 0 ? "-" + positive : positive;
	}
}
