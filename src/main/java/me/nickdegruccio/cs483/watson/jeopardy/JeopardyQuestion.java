package me.nickdegruccio.cs483.watson.jeopardy;

public class JeopardyQuestion {

	private String category;
	private String clue;
	private String answer;

	public JeopardyQuestion(String category, String clue, String answer) {
		this.category = category;
		this.clue = clue;
		this.answer = answer;
	}

	public String getCategory() {
		return category;
	}

	public String getClue() {
		return clue;
	}

	public String getAnswer() {
		return answer;
	}

	@Override
	public String toString() {
		return "JeopardyQuestion{" +
				"category='" + category + '\'' +
				", clue='" + clue + '\'' +
				", answer='" + answer + '\'' +
				'}';
	}
}
