package me.nickdegruccio.cs483.watson.jeopardy;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class JeopardyReader {

	public List<JeopardyQuestion> readAll() throws Exception {
		List<JeopardyQuestion> questions = new ArrayList<>();
		ClassLoader loader = getClass().getClassLoader();
		File file = new File(loader.getResource("questions.txt").getFile());
		Scanner scanner = new Scanner(file);

		while(scanner.hasNext()) {
			JeopardyQuestion question = new JeopardyQuestion(
					scanner.nextLine(),
					scanner.nextLine(),
					scanner.nextLine()
			);
			questions.add(question);
			scanner.nextLine();
		}

		return questions;
	}
}
