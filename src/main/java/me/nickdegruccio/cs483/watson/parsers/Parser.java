package me.nickdegruccio.cs483.watson.parsers;

public interface Parser {

	String name();

	/**
	 * Annotates and styles the input content into the target style of the parser
	 *
	 * @param content The content to apply the parser to
	 * @return The updated content for the input string
	 */
	String apply(String content);

	int getCorrect();

	void setCorrect(int correct);
}
