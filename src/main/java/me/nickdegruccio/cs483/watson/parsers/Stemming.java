package me.nickdegruccio.cs483.watson.parsers;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import me.nickdegruccio.cs483.watson.StopwordRemover;
import org.tartarus.snowball.ext.PorterStemmer;

import java.util.List;
import java.util.Properties;

public class Stemming implements Parser {
	private StanfordCoreNLP pipeline;
	private PorterStemmer stemmer = new PorterStemmer();
	private int correct = 0;

	public Stemming() {
		Properties properties = new Properties();
		properties.setProperty("annotators", "tokenize, ssplit, pos");
		this.pipeline = new StanfordCoreNLP(properties);
	}

	@Override
	public String name() {
		return "stemming";
	}

	public String apply(String content) {
		content = new StopwordRemover().removeAllStopWords(content);
		StringBuilder indexed = new StringBuilder();
		Annotation annotation = new Annotation(content);
		this.pipeline.annotate(annotation);
		List<CoreMap> sentences = annotation.get(CoreAnnotations.SentencesAnnotation.class);
		for(CoreMap sentence : sentences) {
			for(CoreLabel token : sentence.get(CoreAnnotations.TokensAnnotation.class)) {
				stemmer.setCurrent(token.word());
				stemmer.stem();
				if(stemmer.getCurrent().equals(".")) {
					indexed.append(stemmer.getCurrent()).append(" ");
				} else {
					indexed.append(" ").append(stemmer.getCurrent());
				}
			}
		}
		return indexed.toString().trim();
	}

	@Override
	public int getCorrect() {
		return this.correct;
	}

	@Override
	public void setCorrect(int correct) {
		this.correct = correct;
	}
}
