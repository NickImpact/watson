package me.nickdegruccio.cs483.watson.wiki;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import me.nickdegruccio.cs483.watson.StopwordRemover;
import me.nickdegruccio.cs483.watson.parsers.Parser;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WikiReader {

	private static final Pattern WIKI_PAGE = Pattern.compile("^\\[\\[(?<page>.+)]]$");
	private static final Pattern WIKI_HEADER = Pattern.compile("^[=]+(?<header>[^=]+)[=]{2,}$");
	private static final Pattern WIKI_TLP = Pattern.compile("\\[tpl].+\\[/tpl]");

	private Document working = null;
	private StringBuilder content = null;

	private Parser parser;

	public WikiReader(Parser parser) {
		this.parser = parser;
	}

	public List<Document> index() throws Exception {
		StopwordRemover swr = new StopwordRemover();
		swr.readStopwords();
		List<Document> documents = new ArrayList<>();
		File wikiDir = new File(getClass().getClassLoader().getResource("wiki").getFile());

		int file = 1;
		for(File pages : wikiDir.listFiles()) {
			int totalDocs = documents.size();
			System.out.print("  " + file++ + "/" + 80 + " (" + pages.getName() + "): ");

			Scanner scanner = new Scanner(pages);
			while(scanner.hasNextLine()) {
				String input = scanner.nextLine().trim();

				if(!input.isEmpty()) {
					Matcher matcher = WIKI_PAGE.matcher(input);
					if (matcher.matches()) {
						if(working != null) {
							String c = content.toString();
							Matcher tlp = WIKI_TLP.matcher(c);
							if(tlp.find()) {
								c = WIKI_TLP.matcher(c).replaceAll("");
							}

							working.add(new TextField("content", parser.apply(c), Field.Store.YES));
							documents.add(working);
						}

						this.working = new Document();
						this.content = new StringBuilder();
						this.working.add(new StringField("page", matcher.group("page"), Field.Store.YES));
					} else {
						Matcher headers = WIKI_HEADER.matcher(input);
						if(headers.matches()) {
							input = headers.group("header");
						}
						content.append(input).append("\n");
					}
				}
			}

			System.out.println("+" + (documents.size() - totalDocs) + " docs");
		}

		System.out.println("  Total documents read: " + documents.size());

		return documents;
	}
}
